# Cordova & Angular

## What do we need?

* **[Node.js](https://nodejs.org/en/)**:
    Download and install from the site. (Node.js version: 6.9.0+, npm version: 3+)

* **[Angular](https://angular.io/)** / **[Angular-CLI](https://angular.io/)**:
    In command prompt or terminal:
    
```shell
        $ npm install -g @angular/cli
```

* **[Cordova](https://cordova.apache.org/)**
    In command prompt or terminal:
    
```shell
        $ npm install -g cordova
```

* **[Android Studio](https://developer.android.com/studio/index.html)** for Android development
    --> Specifically: [Android SDK Manager](https://developer.android.com/studio/intro/update.html#sdk-manager). We don't (need to) use the Android Studio IDE, but we do need the SDK's.

## How to...
Let's assume we start a project in a folder called WebApp. Inside this folder we will start an Angular project, using the Angular CLI. We will also initiate the Cordova app in this same folder.

### Create a new Angular project
Using Angular CLI, we start a project with following commands. After the initiation, don't forget to move inside the created project folder ('WebApp'). `ng new WebApp` creates this project folder and adds a bunch of files that we need for an Angular project. After creating the necessary files, it automatically starts installing the packages specified in *package.json*. We don't need to do anything after this, so we can run the web application with `ng serve`.

```shell
        $ ng new WebApp
        $ cd WebApp
        $ ng serve
```

### Create a new Cordova project
The `cordova create`-command takes 3 parameters:
    1: the folder where the solution will be created 
    2: a reverse domain-style identifier which must be unique 
    3: the application display name
We want to create a folder called '*cordova*' inside our project.

        $ cordova create cordova com.example.webapp WebApp

### Folder structure
This leaves us with the following folder structure:

```
\---WebApp
    |   .angular-cli.json
    |   .editorconfig
    |   .gitignore
    |   karma.conf.js
    |   package.json
    |   protractor.conf.js
    |   README.md
    |   temp.md
    |   tsconfig.json
    |   tslint.json
    |   
    +---cordova
    |   |   .npmignore
    |   |   config.xml
    |   |   package.json
    |   |   
    |   +---hooks
    |   |       README.md
    |   |       
    |   +---platforms
    |   +---plugins
    |   +---res
    |   |   |   README.md
    |   |   |   
    |   |   +---icon
    |   |   |   ...
    |   |   |           
    |   |   \---screen
    |   |       ...
    |   |               
    |   \---www
    |       |   index.html
    |       |   
    |       +---css
    |       |       index.css
    |       |       
    |       +---img
    |       |       logo.png
    |       |       
    |       \---js
    |               index.js
    |               
    +---e2e
    |       app.e2e-spec.ts
    |       app.po.ts
    |       tsconfig.e2e.json
    |
    +---node_modules
    |       ...
    |       
    \---src
        |   favicon.ico
        |   index.html
        |   main.ts
        |   polyfills.ts
        |   styles.css
        |   test.ts
        |   tsconfig.app.json
        |   tsconfig.spec.json
        |   typings.d.ts
        |   
        +---app
        |       app.component.css
        |       app.component.html
        |       app.component.spec.ts
        |       app.component.ts
        |       app.module.ts
        |       
        +---assets
        |       .gitkeep
        |       
        \---environments
                environment.prod.ts
                environment.ts
```

### Add the cordova platforms
We have to specify which platforms will be supported. Move to the *WebApp/cordova* folder inside our project (remember: we have a Cordova project inside our Angular project, which means we have to be inside the Cordova project to run Cordova commands). In our case, we want to build an application for Android.

```shell
        $ cd cordova
        $ cordova add platform android
```

Other platform options are:
* ios
* windows
* wp8
* blackberry10
* firefoxos
* amazon-fireos

### Build the Angular project
To build the web application, we run `ng build`. This generates a build in the *WebApp/dist/* directory.

```shell
        $ ng build
```

### Move the build to *www*
Delete the contents of *WebApp/cordova/www/* and copy/paste the contents from *WebApp/dist/* to the empty *www* directory.

### Prepare the *index.html*
We need to change the app base in our *WebApp/cordova/index.html*.

```HTML
        <!-- ... -->
            <base href="/">
        <!-- ... -->
```
Change the above to the following: 
```HTML
        <!-- ... -->
            <base href=".">
        <!-- ... -->
```

### Build the cordova project
We want to make a build for android, so inside the cordova directory...

```shell
        $ cordova build android
```

This is where our projects starts using Android stuff (sdk's...). This can be a bit tricky to configure, but the internet is already full of tutorials and threads on this subject.

### Run the cordova project
To deploy the application we use `cordova run [platform]`. To run the app on an emulator, we can add the `--emulator` flag.

```shell
        $ cordova run android --emulator
```

This is where our projects starts using Android stuff (sdk's...). This can be a bit tricky to configure, but the internet is already full of tutorials and threads on this subject.

### Done

That's it. We now have a mobile application, made from an Angular application. It is a very simple application right now and it does nothing except tell us 'app works!', but that's where the development starts. You can start adding functionality and styling to your web application like always and package it to a mobile app by placing your build in the *www* folder.

![App result](./result.png)

#### Note: Plugins
In cordova we can use native features by adding [plugins](https://cordova.apache.org/plugins/). This is done as follows:

```
        $ cordova plugin add [plugin]
```